import BlogPostService from '../services/PostsService';
import CommentService from '../services/CommentsService';

/**
 * This file contains all the redux actions used within this app.
 * These actions will be handled by the main reducer.
 */

//Sync action creators and types
export const SELECT_POST = 'SELECT_POST';
export const selectPost = (postId) => ({
  type: SELECT_POST,
  postId: postId
});

export const REQUEST_POSTS = 'REQUEST_POSTS';
//This action creator is exposed so it can be unit tested
export const requestPosts = () => ({
  type: REQUEST_POSTS
});

export const RECEIVE_POSTS = 'RECEIVE_POSTS';
//This action creator is exposed so it can be unit tested
export const receivePosts = (posts) => ({
    type: RECEIVE_POSTS,
    items: posts
});

export const REQUEST_COMMENTS = 'REQUEST_COMMENTS';
//This action creator is exposed so it can be unit tested
export const requestComments = (postId) => ({
  type: REQUEST_COMMENTS,
  postId: postId
});

export const RECEIVE_COMMENTS = 'RECEIVE_COMMENTS';
//This action creator is exposed so it can be unit tested
export const receiveComments = (comments) => ({
  type: RECEIVE_COMMENTS,
  items: comments
});

export const UPDATE_COMMENT_USER = 'UPDATE_COMMENT_USER';
export const updateCommentUser = (commentUser) => ({
  type: UPDATE_COMMENT_USER,
  commentUser: commentUser
});

export const UPDATE_COMMENT_CONTENT = 'UPDATE_COMMENT_CONTENT';
export const updateCommentContent = (commentContent) => ({
  type: UPDATE_COMMENT_CONTENT,
  commentContent: commentContent
});

export const REQUEST_TO_POST_A_COMMENT = 'REQUEST_TO_POST_A_COMMENT';
export const requestToPostAComment = (comment) => ({
  type: REQUEST_TO_POST_A_COMMENT,
  comment: comment
});

export const NOTIFY_COMMENT_HAS_BEEN_POSTED = 'NOTIFY_COMMENT_HAS_BEEN_POSTED';
//This action creator is exposed so it can be unit tested
export const notifyCommentHasBeenPosted = () => ({
  type: NOTIFY_COMMENT_HAS_BEEN_POSTED
});

export const NOTIFY_COMMENT_FIELDS_EMPTY = 'NOTIFY_COMMENT_FIELDS_EMPTY';
//This action creator is exposed so it can be unit tested
export const notifyCommentFieldsEmpty = () => ({
  type: NOTIFY_COMMENT_FIELDS_EMPTY
});

export const SELECT_COMMENT_TO_REPLY = "SELECT_COMMENT_TO_REPLY";
export const selectCommentToReply = (selectedCommentId) => ({
  type: SELECT_COMMENT_TO_REPLY,
  selectedCommentId: selectedCommentId
});

// Async thunk action creators
// This thunk action creators won't have a corresponding handler in the reducer since
// it only triggers synchronous actions that are handled by the main reducer.
// However they need to be exposed so they can be triggered from components.

/**
 * Action creator used to fetch posts.
 */
export const fetchPosts = () => {
  return (dispatch) => {
    dispatch(requestPosts());
    return BlogPostService.fetchAll({
      success: (payload) => {
        dispatch(receivePosts(BlogPostService.sortByDateNewestFirst(payload)));
      }
    });
  };
};

/**
 * Action creator used to post a comment.
 *
 * @param comment {Object} object containing the information required to post a comment
 * @param comment.commentUser {String} contains the user name of who is posting
 * @param comment.commentContent {String} contains the content of the comment being posted
 * @return promise from postAComment method
 */
export const postAComment = (comment) => {
  return (dispatch, getState) => {
    if (!comment.commentUser || !comment.commentContent) {
      dispatch(notifyCommentFieldsEmpty());
      return new Promise((resolve) => {
        resolve();
      });
    }

    dispatch(requestToPostAComment(comment));
    const currentDate = new Date();
    return CommentService.postAComment({
      payload: {
        postId: getState().comments.postId,
        user: comment.commentUser,
        content: comment.commentContent,
        parent_id: comment.selectedCommentId,
        date: currentDate.toISOString().split('T')[0],
        fullDate: currentDate.toISOString()
      },
      success: () => {
        console.log('Comment posted successfully');
        dispatch(notifyCommentHasBeenPosted());
        dispatch(fetchComments(getState().comments.postId));
      }
    })
  };
};

/**
 * Action creator used to fetch comments related to a post.
 *
 * @param postId {String} string containing the id of the post to be fetched
 * @return promise from fetchAllByPostId method
 */
export const fetchComments = (postId) => {
  return (dispatch, getState) => {
    dispatch(requestComments(postId));
    return CommentService.fetchAllByPostId({
      postId: getState().comments.postId,
      success: (payload) => {

        let mappedComments = CommentService.mapComments(payload);

        dispatch(receiveComments(mappedComments));
      }
    })
  };
};