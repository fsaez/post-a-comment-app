import {
  selectPost,
  requestPosts,
  receivePosts,
  requestComments,
  receiveComments,
  updateCommentUser,
  updateCommentContent,
  requestToPostAComment,
  notifyCommentHasBeenPosted,
  notifyCommentFieldsEmpty,
  fetchPosts,
  postAComment,
  fetchComments,
  selectCommentToReply
} from './actions';

import BlogPostService from '../services/PostsService';
import CommentService from '../services/CommentsService';

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

describe('actions.js', () => {
  describe('Testing sync action creators', () => {
    it('should create SELECT_POST action properly', () => {
      const expectedAction = {
        type: 'SELECT_POST',
        postId: '1'
      };

      const actualAction = selectPost('1');

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create REQUEST_POSTS action properly', () => {
      const expectedAction = {
        type: 'REQUEST_POSTS'
      };

      const actualAction = requestPosts();

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create RECEIVE_POSTS action properly', () => {
      const expectedAction = {
        type: 'RECEIVE_POSTS',
        items: [{ content: 'a' }, { content: 'b' }]
      };

      const actualAction = receivePosts([{ content: 'a' }, { content: 'b' }]);

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create REQUEST_COMMENTS action properly', () => {
      const expectedAction = {
        type: 'REQUEST_COMMENTS',
        postId: '1'
      };

      const actualAction = requestComments('1');

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create RECEIVE_COMMENTS action properly', () => {
      const expectedAction = {
        type: 'RECEIVE_COMMENTS',
        items: [{ content: 'a' }, { content: 'b' }]
      };

      const actualAction = receiveComments([{ content: 'a' }, { content: 'b' }]);

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create UPDATE_COMMENT_USER action properly', () => {
      const expectedAction = {
        type: 'UPDATE_COMMENT_USER',
        commentUser: 'hi'
      };

      const actualAction = updateCommentUser('hi');

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create UPDATE_COMMENT_CONTENT action properly', () => {
      const expectedAction = {
        type: 'UPDATE_COMMENT_CONTENT',
        commentContent: 'hi'
      };

      const actualAction = updateCommentContent('hi');

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create REQUEST_TO_POST_A_COMMENT action properly', () => {
      const expectedAction = {
        type: 'REQUEST_TO_POST_A_COMMENT',
        comment: 'hi'
      };

      const actualAction = requestToPostAComment('hi');

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create NOTIFY_COMMENT_HAS_BEEN_POSTED action properly', () => {
      const expectedAction = {
        type: 'NOTIFY_COMMENT_HAS_BEEN_POSTED'
      };

      const actualAction = notifyCommentHasBeenPosted();

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create NOTIFY_COMMENT_FIELDS_EMPTY action properly', () => {
      const expectedAction = {
        type: 'NOTIFY_COMMENT_FIELDS_EMPTY'
      };

      const actualAction = notifyCommentFieldsEmpty();

      expect(actualAction).toEqual(expectedAction);
    });

    it('should create SELECT_COMMENT_TO_REPLY action properly', () => {
      const expectedAction = {
        type: 'SELECT_COMMENT_TO_REPLY',
        selectedCommentId: '1'
      };

      const actualAction = selectCommentToReply('1');

      expect(actualAction).toEqual(expectedAction);
    });
  });

  describe('Testing async action creators', () => {
    const middlewares = [thunk];
    const mockStore = configureMockStore(middlewares);

    const stubbedResponseAllPosts = [
      {
        "id": 1,
        "title": "Blog post #1",
        "author": "Melissa Manges",
        "publish_date": "2016-02-23",
        "slug": "blog-post-1",
        "description": "Utroque denique invenire et has.",
        "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
      }
    ];

    const stubbedResponsePostAComment = {
      "id": 1,
      "title": "Blog post #1",
      "author": "Melissa Manges",
      "publish_date": "2016-02-23",
      "slug": "blog-post-1",
      "description": "Utroque denique invenire et has.",
      "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
    };

    const stubbedResponseAllComments = [
      {
        "id": 1,
        "postId": 1,
        "parent_id": null,
        "user": "Amelia",
        "date": "2016-02-23",
        "content": "Nulla in nulla vel nisi faucibus scelerisque. Donec quis tortor."
      }
    ];

    const initialState = {
      posts: {
        isFetching: false,
        items: []
      },
      comments: {
        message: null,
        commentUser: '',
        commentContent: '',
        postId: '1',
        isFetching: false,
        items: []
      }
    };

    it('should create fetchPosts async action properly', () => {

      spyOn(BlogPostService, 'fetchAll').and.callFake((options) => {
        return new Promise((resolve) => {
          options.success(stubbedResponseAllPosts);
          resolve();
        });
      });

      const expectedActions = [
        { type: 'REQUEST_POSTS' },
        { type: 'RECEIVE_POSTS', items: [{
          "id": 1,
          "title": "Blog post #1",
          "author": "Melissa Manges",
          "publish_date": "2016-02-23",
          "slug": "blog-post-1",
          "description": "Utroque denique invenire et has.",
          "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
        }] }
      ];

      const store = mockStore(initialState);

      return store.dispatch(fetchPosts()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should create postAComment async action properly', () => {
      spyOn(CommentService, 'postAComment').and.callFake((options) => {
        return new Promise((resolve) => {
          options.success(stubbedResponsePostAComment);
          resolve();
        });
      });

      spyOn(CommentService, 'fetchAllByPostId').and.callFake((options) => {
        return new Promise((resolve) => {
          options.success(stubbedResponseAllComments);
          resolve();
        });
      });

      const expectedActions = [
        { type: 'REQUEST_TO_POST_A_COMMENT', comment: { commentUser: 'Juan', commentContent: 'Hi content' } },
        { type: 'NOTIFY_COMMENT_HAS_BEEN_POSTED' },
        { type: 'REQUEST_COMMENTS', postId: '1' },
        { type: 'RECEIVE_COMMENTS', items: [{
          "id": 1,
          "postId": 1,
          "parent_id": null,
          "user": "Amelia",
          "date": "2016-02-23",
          "content": "Nulla in nulla vel nisi faucibus scelerisque. Donec quis tortor."
        }] }
      ];

      const store = mockStore(initialState);

      return store.dispatch(postAComment({ commentUser: 'Juan', commentContent: 'Hi content' })).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should not post a comment when postAComment is triggered with wrong parameters', () => {
      spyOn(CommentService, 'postAComment').and.callFake((options) => {
        return new Promise((resolve) => {
          options.success(stubbedResponsePostAComment);
          resolve();
        });
      });

      const expectedActions = [
        { type: 'NOTIFY_COMMENT_FIELDS_EMPTY' },
        { type: 'NOTIFY_COMMENT_FIELDS_EMPTY' },
        { type: 'NOTIFY_COMMENT_FIELDS_EMPTY' }
      ];

      const store = mockStore(initialState);

      //First we try to post a comment with commentUser and commentContent both empty
      return store.dispatch(postAComment({ commentUser: '', commentContent: '' })).then(() => {
        //Then we try to post a comment with commentUser empty
        store.dispatch(postAComment({ commentUser: '', commentContent: 'Hi' })).then(() => {
          //Then we try to post a comment with commentContent empty
          store.dispatch(postAComment({ commentUser: 'Juan', commentContent: '' })).then(() => {
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions);
          });
        });
      });
    });

    it('should create fetchComments async action properly', () => {

      spyOn(CommentService, 'fetchAllByPostId').and.callFake((options) => {
        return new Promise((resolve) => {
          options.success(stubbedResponseAllComments);
          resolve();
        });
      });

      const expectedActions = [
        { type: 'REQUEST_COMMENTS', postId: '1' },
        { type: 'RECEIVE_COMMENTS', items: [{
          "id": 1,
          "postId": 1,
          "parent_id": null,
          "user": "Amelia",
          "date": "2016-02-23",
          "content": "Nulla in nulla vel nisi faucibus scelerisque. Donec quis tortor."
        }] }
      ];

      const store = mockStore(initialState);

      return store.dispatch(fetchComments('1')).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});