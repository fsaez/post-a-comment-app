import { REQUEST_POSTS,
  RECEIVE_POSTS,
  REQUEST_COMMENTS,
  RECEIVE_COMMENTS,
  UPDATE_COMMENT_USER,
  UPDATE_COMMENT_CONTENT,
  REQUEST_TO_POST_A_COMMENT,
  NOTIFY_COMMENT_HAS_BEEN_POSTED,
  SELECT_POST,
  NOTIFY_COMMENT_FIELDS_EMPTY,
  SELECT_COMMENT_TO_REPLY
} from '../actions/actions';
import { combineReducers } from 'redux';

const initialState = {
  posts: {
    isFetching: false,
    items: []
  },
  comments: {
    selectedCommentId: null,
    message: null,
    commentUser: '',
    commentContent: '',
    postId: null,
    isFetching: false,
    items: []
  }
};

const posts = (state = initialState.posts, action) => {
  switch (action.type) {
    case REQUEST_POSTS:
      return { ...state, isFetching: true };
    case RECEIVE_POSTS:
      return { ...state, isFetching: false, items: action.items };
    default:
      return state;
  }
};

const comments = (state = initialState.comments, action) => {
  switch (action.type) {
    case SELECT_POST:
      return { ...state, postId: action.postId, message: null };
    case REQUEST_COMMENTS:
      return { ...state, isFetching: true };
    case RECEIVE_COMMENTS:
      return { ...state, isFetching: false, items: action.items };
    case UPDATE_COMMENT_USER:
      return { ...state, commentUser: action.commentUser, message: null };
    case UPDATE_COMMENT_CONTENT:
      return { ...state, commentContent: action.commentContent, message: null };
    case REQUEST_TO_POST_A_COMMENT:
      return { ...state, isFetching: true, comment: action.comment };
    case NOTIFY_COMMENT_HAS_BEEN_POSTED:
      return { ...state, isFetching: false, message: { text: 'Your post has been posted.', type: 'success' }, commentUser: '', commentContent: '', selectedCommentId: null };
    case NOTIFY_COMMENT_FIELDS_EMPTY:
      return { ...state, isFetching: false, message: { text: 'Check your username or the content of your comment.', type: 'error' } };
    case SELECT_COMMENT_TO_REPLY:
      return { ...state, selectedCommentId: action.selectedCommentId, message: null };
    default:
      return state;
  }
};

const reactApp = combineReducers({
  posts,
  comments
});

export default reactApp;