import BlogPostService from './PostsService';

const stubbedResponseAll = [
  {
    "id": 1,
    "title": "Blog post #1",
    "author": "Melissa Manges",
    "publish_date": "2016-02-23",
    "slug": "blog-post-1",
    "description": "Utroque denique invenire et has.",
    "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
  },
  {
    "id": 2,
    "title": "Blog post #2",
    "author": "Olene Ogan",
    "publish_date": "2016-03-16",
    "slug": "blog-post-2",
    "description": "Ex legere perpetua electram vim, per nisl inermis quaestio ea.",
    "content": "<p>Ex legere perpetua electram vim, per nisl inermis quaestio ea. Everti adolescens ut nec. Quod labitur assueverit vis at, sea an erat modus delicata.</p> <p>Dico omnesque epicurei te vix. Tota verterem temporibus eu quo, eu iudicabit repudiandae sea. Elitr nihil gloriatur vis in.</p>"
  },
  {
    "id": 3,
    "title": "Blog post #3",
    "author": "Annemarie Axelrod",
    "publish_date": "2016-03-28",
    "slug": "blog-post-3",
    "description": "Sea ne harum reformidans conclusionemque.",
    "content": "<p>Sea ne harum reformidans conclusionemque. Eu eirmod adversarium definitiones pri, id brute option convenire sed.</p> <p>Id per porro tritani, mei ut assum persius prompta. Nominavi eligendi cu mea. Utinam consul theophrastus te sit, denique platonem assentior te pri. Nam id enim case iracundia.</p>"
  }
];

const stubbedResponseAllSorted = [
  {
    "id": 3,
    "title": "Blog post #3",
    "author": "Annemarie Axelrod",
    "publish_date": "2016-03-28",
    "slug": "blog-post-3",
    "description": "Sea ne harum reformidans conclusionemque.",
    "content": "<p>Sea ne harum reformidans conclusionemque. Eu eirmod adversarium definitiones pri, id brute option convenire sed.</p> <p>Id per porro tritani, mei ut assum persius prompta. Nominavi eligendi cu mea. Utinam consul theophrastus te sit, denique platonem assentior te pri. Nam id enim case iracundia.</p>"
  },
  {
    "id": 2,
    "title": "Blog post #2",
    "author": "Olene Ogan",
    "publish_date": "2016-03-16",
    "slug": "blog-post-2",
    "description": "Ex legere perpetua electram vim, per nisl inermis quaestio ea.",
    "content": "<p>Ex legere perpetua electram vim, per nisl inermis quaestio ea. Everti adolescens ut nec. Quod labitur assueverit vis at, sea an erat modus delicata.</p> <p>Dico omnesque epicurei te vix. Tota verterem temporibus eu quo, eu iudicabit repudiandae sea. Elitr nihil gloriatur vis in.</p>"
  },
  {
    "id": 1,
    "title": "Blog post #1",
    "author": "Melissa Manges",
    "publish_date": "2016-02-23",
    "slug": "blog-post-1",
    "description": "Utroque denique invenire et has.",
    "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
  }
];

const stubbedResponsePostId1 = {
  "id": 1,
  "title": "Blog post #1",
  "author": "Melissa Manges",
  "publish_date": "2016-02-23",
  "slug": "blog-post-1",
  "description": "Utroque denique invenire et has.",
  "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
};


describe('BlogPostService', () => {

  it('should fetch all blog entries', (done) => {
    let actualPayload;
    const expectedPayload = [
      {
        "id": 1,
        "title": "Blog post #1",
        "author": "Melissa Manges",
        "publish_date": "2016-02-23",
        "slug": "blog-post-1",
        "description": "Utroque denique invenire et has.",
        "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
      },
      {
        "id": 2,
        "title": "Blog post #2",
        "author": "Olene Ogan",
        "publish_date": "2016-03-16",
        "slug": "blog-post-2",
        "description": "Ex legere perpetua electram vim, per nisl inermis quaestio ea.",
        "content": "<p>Ex legere perpetua electram vim, per nisl inermis quaestio ea. Everti adolescens ut nec. Quod labitur assueverit vis at, sea an erat modus delicata.</p> <p>Dico omnesque epicurei te vix. Tota verterem temporibus eu quo, eu iudicabit repudiandae sea. Elitr nihil gloriatur vis in.</p>"
      },
      {
        "id": 3,
        "title": "Blog post #3",
        "author": "Annemarie Axelrod",
        "publish_date": "2016-03-28",
        "slug": "blog-post-3",
        "description": "Sea ne harum reformidans conclusionemque.",
        "content": "<p>Sea ne harum reformidans conclusionemque. Eu eirmod adversarium definitiones pri, id brute option convenire sed.</p> <p>Id per porro tritani, mei ut assum persius prompta. Nominavi eligendi cu mea. Utinam consul theophrastus te sit, denique platonem assentior te pri. Nam id enim case iracundia.</p>"
      }
    ];

    spyOn(BlogPostService, 'fetchAll').and.callFake((options) => {
      options.success(stubbedResponseAll);
    });

    BlogPostService.fetchAll({
      success: (payload) => {
        actualPayload = payload;
        expect(actualPayload).toEqual(expectedPayload);
        done();
      }
    });
  });

  it('should fetch entry at specific position', (done) => {
    const expectedPayload = {
      "id": 1,
      "title": "Blog post #1",
      "author": "Melissa Manges",
      "publish_date": "2016-02-23",
      "slug": "blog-post-1",
      "description": "Utroque denique invenire et has.",
      "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
    };

    let actualPayload;

    spyOn(BlogPostService, 'fetchByPostId').and.callFake((options) => {
      options.success(stubbedResponsePostId1);
    });

    BlogPostService.fetchByPostId({
      postId: 1,
      success: (payload) => {
        actualPayload = payload;
        expect(actualPayload).toEqual(expectedPayload);
        done();
      }
    });
  });

  it('fetches sorted blog entries from newest', (done) => {
    let actualPayload;
    const expectedPayload = [
      {
        "id": 3,
        "title": "Blog post #3",
        "author": "Annemarie Axelrod",
        "publish_date": "2016-03-28",
        "slug": "blog-post-3",
        "description": "Sea ne harum reformidans conclusionemque.",
        "content": "<p>Sea ne harum reformidans conclusionemque. Eu eirmod adversarium definitiones pri, id brute option convenire sed.</p> <p>Id per porro tritani, mei ut assum persius prompta. Nominavi eligendi cu mea. Utinam consul theophrastus te sit, denique platonem assentior te pri. Nam id enim case iracundia.</p>"
      },
      {
        "id": 2,
        "title": "Blog post #2",
        "author": "Olene Ogan",
        "publish_date": "2016-03-16",
        "slug": "blog-post-2",
        "description": "Ex legere perpetua electram vim, per nisl inermis quaestio ea.",
        "content": "<p>Ex legere perpetua electram vim, per nisl inermis quaestio ea. Everti adolescens ut nec. Quod labitur assueverit vis at, sea an erat modus delicata.</p> <p>Dico omnesque epicurei te vix. Tota verterem temporibus eu quo, eu iudicabit repudiandae sea. Elitr nihil gloriatur vis in.</p>"
      },
      {
        "id": 1,
        "title": "Blog post #1",
        "author": "Melissa Manges",
        "publish_date": "2016-02-23",
        "slug": "blog-post-1",
        "description": "Utroque denique invenire et has.",
        "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>"
      }
    ];

    spyOn(BlogPostService, 'fetchAll').and.callFake((options) => {
      options.success(stubbedResponseAllSorted);
    });

    BlogPostService.fetchAll({
      success: (payload) => {
        actualPayload = BlogPostService.sortByDateNewestFirst(payload);
        expect(actualPayload).toEqual(expectedPayload);
        done();
      }
    });
  });

  it('supports fetch function', () => {
    expect(typeof fetch).toEqual('function');
  });
});