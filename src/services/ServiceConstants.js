const BASE_URL = 'http://localhost:9001';
const POSTS_URL = BASE_URL + '/posts';

const getPostACommentURL = (postId) => (
  getCommentsURL(postId)
);

const getCommentsURL = (postId) => (
  POSTS_URL + '/' + postId + '/comments'
);

const getCommentURL = (postId, commentId) => (
  POSTS_URL + '/' + postId + '/comments/' + commentId
);

const getPostURL = (postId) => (
  POSTS_URL + '/' + postId
);

export default {
  BASE_URL: BASE_URL,
  POSTS_URL: POSTS_URL,
  getCommentsURL: getCommentsURL,
  getCommentURL: getCommentURL,
  getPostURL: getPostURL,
  getPostACommentURL: getPostACommentURL
};