import BaseServiceProvider from './BaseServiceProvider';
import ServiceConstants from './ServiceConstants';
import ArrayUtilities from '../utils/ArrayUtilities';

/**
 * This service is used to handle data related to comments.
 */

/**
 * Method used to fetch all the comments for a specific blog post
 *
 * @param options {Object} object used to pass parameters to the method
 * @param options.success {Function} callback triggered when the service call succeeds. It will be called using the service result as a parameter
 * @param options.postId {String} id of the post which the comment belongs to
 * @return {Promise} promise from fetch method
 */
const fetchAllByPostId = (options) => {
  if (options.postId) {
    options.url = ServiceConstants.getCommentsURL(options.postId);
    return BaseServiceProvider.fetch(options);
  } else {
    BaseServiceProvider.throwParametersError();
  }
};

/**
 * Method used to post a comment on an specific blog post
 *
 * @param options {Object} object to call the postAComment method
 * @param options.success {Function} callback triggered when the service call succeeds
 * @param options.payload {Object} object that contains the data related to the comment to be posted
 * @param options.postId {String} id of the post which the comment belongs to
 * @param options.payload.user {String} user name of the user posting the comment
 * @param options.payload.content {String} comment's content
 * @param options.payload.date date when the comment is being posted (e.g. 2018-01-05)
 * @param options.payload.fullDate timestamp for when the comment was posted (e.g. 2018-01-05T22:31:33.781Z)
 * @return {Promise} promise from basePost method
 */
const postAComment = (options) => {
  options.url = ServiceConstants.getPostACommentURL(options.payload.postId);
  return BaseServiceProvider.basePost(options);
};

/**
 * Method used to sort comments from oldest to newest
 *
 * @param comments {Array} array of comments
 * @return {Array} sorted array
 */
const sortByDateOldestFirst = (comments) => (
  //we could use just date which is an existing field but this won't guarantee that the comments will be in the order
  //they were added
  ArrayUtilities.sortByAttributeLowestFirst(comments, 'fullDate')
);

/**
 * This method maps comments from the flat list to a structured list depending on parent_id
 * that allows to visualize comment replies
 * @param rawComments
 * @returns {Array}
 */
const mapComments = (rawComments) => {
  let mappedComments = [];
  rawComments.forEach(comment => {
    if (!comment.parent_id) {
      //first we find the top level comments i.e. those that don't have a parent_id associated
      mappedComments.push(comment);
      findChildren(rawComments, comment);
    }
  });

  //finally we sort the items
  mappedComments = sortChildren(mappedComments);
  return mappedComments;
};

/**
 * Recursive Method that alo
 * @param comments
 * @param currentComment
 */
const findChildren = (comments, currentComment) => {
  comments.forEach((comment) => {
    if (comment.parent_id === currentComment.id) {
      findChildren(comments, comment);
      if (!currentComment.children) currentComment.children = [];
      currentComment.children.push(comment);
    }
  });
};

/**
 * Recursive method that sorts comments depending on their parent_id
 * @param comments
 * @returns {Array}
 */
const sortChildren = (comments) => {
  comments.forEach((comment) => {
    if (comment.children) {
      sortChildren(comment.children);
    }
  });

  return sortByDateOldestFirst(comments);
};

export default {
  fetchAllByPostId: fetchAllByPostId,
  postAComment: postAComment,
  sortByDateOldestFirst: sortByDateOldestFirst,
  mapComments: mapComments
};