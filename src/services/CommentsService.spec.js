import CommentService from "./CommentsService";

const stubbedResponseAllComments = [
  {
    "id": 1,
    "postId": 1,
    "parent_id": null,
    "user": "Amelia",
    "date": "2016-02-23",
    "content": "Nulla in nulla vel nisi faucibus scelerisque. Donec quis tortor."
  },
  {
    "id": 2,
    "postId": 1,
    "parent_id": 1,
    "user": "Jake",
    "date": "2016-02-23",
    "content": "Cras lectus nisl, scelerisque quis elit ut, luctus scelerisque purus."
  },
  {
    "id": 3,
    "postId": 1,
    "parent_id": 2,
    "user": "Amelia",
    "date": "2016-02-24",
    "content": "Cras est nunc, tempus eget risus vitae, vulputate ornare magna."
  }
];

const stubbedResponsePostAComment = {
  "postId": "2",
  "parent_id": null,
  "user": "Juan",
  "date": "2016-03-17",
  "content": "Nunc facilisis nisi vitae dapibus sodales. Proin vitae nunc turpis."
};

describe('CommentService', () => {
  it('should fetch all comments', (done) => {
    const expectedPayload = [
      {
        "id": 1,
        "postId": 1,
        "parent_id": null,
        "user": "Amelia",
        "date": "2016-02-23",
        "content": "Nulla in nulla vel nisi faucibus scelerisque. Donec quis tortor."
      },
      {
        "id": 2,
        "postId": 1,
        "parent_id": 1,
        "user": "Jake",
        "date": "2016-02-23",
        "content": "Cras lectus nisl, scelerisque quis elit ut, luctus scelerisque purus."
      },
      {
        "id": 3,
        "postId": 1,
        "parent_id": 2,
        "user": "Amelia",
        "date": "2016-02-24",
        "content": "Cras est nunc, tempus eget risus vitae, vulputate ornare magna."
      }
    ];

    spyOn(CommentService, 'fetchAllByPostId').and.callFake((options) => {
      options.success(stubbedResponseAllComments);
    });

    let actualPayload;
    CommentService.fetchAllByPostId({
      postId: 1,
      success: (payload) => {
        actualPayload = payload;
        expect(actualPayload).toEqual(expectedPayload);
        done();
      }
    });
  });

  it('should successfully post a comment', (done) => {
    let options = {
      payload: {
        "postId": "2",
        "parent_id": null,
        "user": "Juan",
        "date": "2016-03-17",
        "content": "Nunc facilisis nisi vitae dapibus sodales. Proin vitae nunc turpis."
      },
      success: (result) => {
        //successful
        console.log("*** success");
        expect(result.user).toBe(options.payload.user);
        done();
      }
    };

    spyOn(CommentService, 'postAComment').and.callFake((options) => {
      options.success(stubbedResponsePostAComment);
    });

    CommentService.postAComment(options);
  });

  it('Maps comments properly based on their parent_id', () => {
    const inputPayload = [
      {
        "id": 1,
        "postId": 1,
        "parent_id": null,
        "user": "Amelia",
        "date": "2016-02-23",
        "content": "Nulla in nulla vel nisi faucibus scelerisque. Donec quis tortor."
      },
      {
        "id": 2,
        "postId": 1,
        "parent_id": 1,
        "user": "Jake",
        "date": "2016-02-23",
        "content": "Cras lectus nisl, scelerisque quis elit ut, luctus scelerisque purus."
      },
      {
        "id": 3,
        "postId": 1,
        "parent_id": 2,
        "user": "Amelia",
        "date": "2016-02-24",
        "content": "Cras est nunc, tempus eget risus vitae, vulputate ornare magna."
      }
    ];

    const expectedPayload = [
      {
        "id": 1,
        "postId": 1,
        "parent_id": null,
        "user": "Amelia",
        "date": "2016-02-23",
        "content": "Nulla in nulla vel nisi faucibus scelerisque. Donec quis tortor.",
        "children": [{
          "id": 2,
          "postId": 1,
          "parent_id": 1,
          "user": "Jake",
          "date": "2016-02-23",
          "content": "Cras lectus nisl, scelerisque quis elit ut, luctus scelerisque purus.",
          "children": [{
            "id": 3,
            "postId": 1,
            "parent_id": 2,
            "user": "Amelia",
            "date": "2016-02-24",
            "content": "Cras est nunc, tempus eget risus vitae, vulputate ornare magna."
          }]
        }]
      }
    ];

    const actualPayload = CommentService.mapComments(inputPayload);
    expect(actualPayload).toEqual(expectedPayload);
  });
});