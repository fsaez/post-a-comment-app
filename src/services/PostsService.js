import BaseServiceProvider from './BaseServiceProvider';
import ServiceConstants from './ServiceConstants';
import ArrayUtilities from '../utils/ArrayUtilities';

/**
 * This service is used to handle data related to posts.
 */

/**
 * Method used to fetch a specific post by its id
 *
 * @param options {Object} object used to pass parameters to the method
 * @param options.success {Function} callback triggered when the service call succeeds. It will be called using the service result as a parameter
 * @param options.postId {String} id used to fetch an specific post
 * @return {Promise} promise from fetch method
 */
const fetchByPostId = (options) => {
  if (options.postId) {
    options.url = ServiceConstants.getPostURL(options.postId);
    return BaseServiceProvider.fetch(options);
  } else {
    BaseServiceProvider.throwParametersError();
  }
};

/**
 * Method used to sort posts from newest to oldest
 *
 * @param posts {Array} array of posts
 * @return {Array} sorted array
 */
const sortByDateNewestFirst = (posts) => (
  ArrayUtilities.sortByAttributeHighestFirst(posts, 'publish_date')
);

/**
 * Method used to fetch all posts
 *
 * @param options {Object} object used to pass parameters to the method
 * @param options.success {Function} callback triggered when the service call succeeds. It will be called using the service result as a parameter
 * @return {Promise} promise from fetch method
 */
const fetchAll = (options) => {
  options.url = ServiceConstants.POSTS_URL;
  return BaseServiceProvider.fetch(options);
};

export default {
  fetchAll: fetchAll,
  fetchByPostId: fetchByPostId,
  sortByDateNewestFirst: sortByDateNewestFirst
};