/**
 * @overview Home page.  Renders static content.
 */
import React from 'react';

const Home = () => <div>
  <div>
    <p className="display-4">Welcome! But this is not the blog page.</p>
  </div>
</div>;

export default Home;
