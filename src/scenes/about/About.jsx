/**
 * @overview About page.  Renders static content.
 */
import React from 'react';

const About = () => <div>
  <div>
    <h2>About us</h2>
    <p>This is not the blog page either, keep looking.</p>
  </div>

</div>;

export default About;
