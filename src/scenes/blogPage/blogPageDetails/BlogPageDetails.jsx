import React from "react";
import CommentList from "components/commentList";
import PageTitle from "components/pageTitle/PageTitle";
import PostDetails from 'components/postDetails';

const BlogPageDetails = (props) => (
  <div>
    <PageTitle>Post details</PageTitle>
    <PostDetails isFetching={ props.isFetching }
                 author={ props.author }
                 title={ props.title }
                 description={ props.description }
                 publishDate={ props.publishDate }
                 content={ props.content }/>
    <div>
      <CommentList postId={ props.postId }/>
    </div>
  </div>
);

export default BlogPageDetails;

