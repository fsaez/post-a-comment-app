import React from "react";
import BlogPageDetails from './BlogPageDetails';
import { fetchComments, fetchPosts, selectPost } from 'actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class BlogPageDetailsContainer extends React.Component {

  constructor(props) {
    super(props);

    this.currentPostId = null;
  }

  componentWillMount() {

    if (!this.props.title) {
      this.props.fetchPosts();
    }

    this.currentPostId = this.props.match.params.postId;

    this.props.selectPost(this.currentPostId);
    this.props.fetchComments(this.currentPostId);
  }

  static getPostByPostId(postId, items) {

    return items.find(post => post['id'].toString() === postId);
  }

  render() {
    return <BlogPageDetails
      title={ this.props.title }
      author={ this.props.author }
      publishDate={ this.props.publishDate }
      description={ this.props.description }
      content={ this.props.content }
      isFetching={ this.props.isFetching }
      postId={ this.currentPostId }
    />;
  }
}

const mapStateToProps = (state) => {
  const currentPost = BlogPageDetailsContainer.getPostByPostId(state.comments.postId, state.posts.items);

  if (!currentPost) {
    return {
      isFetching: state.posts.isFetching
    };
  }

  return {
    title: currentPost['title'],
    author: currentPost['author'],
    publishDate: currentPost['publish_date'],
    description: currentPost['description'],
    content: currentPost['content'],
    isFetching: state.posts.isFetching
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ fetchComments, fetchPosts, selectPost }, dispatch);
};

BlogPageDetailsContainer.propTypes = {
  title: PropTypes.string,
  author: PropTypes.string,
  publish_date: PropTypes.string,
  description: PropTypes.string,
  content: PropTypes.string,
  fetchComments: PropTypes.func,
  fetchPosts: PropTypes.func,
  selectPost: PropTypes.func,
  isFetching: PropTypes.bool
};

export default connect(mapStateToProps, mapDispatchToProps)(BlogPageDetailsContainer);