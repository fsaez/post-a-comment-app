import React from 'react';
import PostList from 'components/postList';
import PageTitle from 'components/pageTitle';

const Blog = () =>
<div className="blog-page">
    <PageTitle>Blog</PageTitle>
    <PostList />
</div>;

export default Blog;
