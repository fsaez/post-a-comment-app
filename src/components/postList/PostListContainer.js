import React from 'react';
import PostList from './PostList';
import { fetchPosts } from '../../actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class PostListContainer extends React.Component {
  componentDidMount() {

    if (this.props.posts.items.length === 0) {
      this.props.fetchPosts();
    }
  }

  render() {
    return <PostList posts={ this.props.posts } />;
  }
}

const mapStateToProps = (state) => ({
  posts: state.posts
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ fetchPosts }, dispatch);
};

PostListContainer.propTypes = {
  fetchPosts: PropTypes.func,
  posts: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(PostListContainer);