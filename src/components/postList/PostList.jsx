import React from "react";
import Post from './components/post';

const PostList = (props) => (
  <div>
    { props.posts.items.map((post) => {
      return <Post key={ post.id } post={ post }/>;
    }) }
  </div>
);

export default PostList;
