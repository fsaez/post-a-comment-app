/**
 * Created by felipesaez on 22/03/18.
 */
import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const PostWrapper = styled.div`
  background-color: #f7f7f7;
  width: 100%;
  color: inherit;
  margin-top: 7px;
  margin-bottom: 7px;
  padding: 10px;
  box-sizing:border-box;
  border-bottom: 1px solid #f9f9f9;
  display: flex;
  position: relative;
  box-shadow: 5px 5px 5px grey;
`;

export const AvatarWrapper = styled.div`
  width: 100px;
`;

export const ContentWrapper = styled.div`
  width: 100%;
`;

export const Title = styled.h4`
  margin-top: 0;
  margin-bottom: 8px;
  color: black;
  border-bottom: 1px solid lightgrey;
`;

export const Date = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  font-size: 10px;
  font-style: italic;
`;

export const StyledLink = styled(Link)`
  color: #2b2d77;
  font-size: 12px;
  position: absolute;
  bottom: 10px;
  right: 10px;
`;