/**
 * Created by felipesaez on 22/03/18.
 */
import React from 'react';
import Avatar from "components/avatar";
import { PostWrapper, AvatarWrapper, ContentWrapper, Title, Date, StyledLink } from './styles';

const Post = (props) => (
  <PostWrapper>
    <AvatarWrapper>
      <Avatar username={ props.post.author } width="40" height="40"/>
    </AvatarWrapper>
    <ContentWrapper>
      <Title>
        { props.post.title }
      </Title>
      <Date>
        { props.post.publish_date }
      </Date>
      <div>
        { props.post.description }
      </div>
      <StyledLink to={ 'blog/' + props.post.id }>
        View Comments
      </StyledLink>
    </ContentWrapper>
  </PostWrapper>
);

export default Post;