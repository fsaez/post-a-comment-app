import React from "react";
import loadingIconImage from './loadingIcon.gif';
import { Img } from './styles';

const LoadingIcon = (props) => (
  <Img alt={ loadingIconImage } className="loading-icon" src={ loadingIconImage } width="25" height="25" />
);

export default LoadingIcon;
