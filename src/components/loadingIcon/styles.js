/**
 * Created by felipesaez on 22/03/18.
 */
import styled from 'styled-components';

export const Img = styled.img`
  display: block;
  margin-left: auto;
  margin-right: auto;
`;