import React from "react";
import PropTypes from 'prop-types';
import { InfoMessageWrapper } from './styles';

const InfoMessage = (props) => (
  <InfoMessageWrapper type={ props.message.type }>{ props.message.text }</InfoMessageWrapper>
);

InfoMessage.propTypes = {
  message: PropTypes.object
};

export default InfoMessage;
