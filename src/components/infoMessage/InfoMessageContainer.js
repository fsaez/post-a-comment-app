/**
 * Created by felipesaez on 22/03/18.
 */
import React from 'react';
import InfoMessage from './InfoMessage';
import { connect } from 'react-redux';

class InfoMessageContainer extends React.Component {
  render() {
    if (this.props.message && this.props.message.type) {
      return <InfoMessage message={ this.props.message } />;
    }

    return null;
  }
}

const mapStateToProps = (state) => ({
  message: state.comments.message
});

export default connect(mapStateToProps)(InfoMessageContainer);