/**
 * Created by felipesaez on 22/03/18.
 */
import styled from 'styled-components';
import errorImage from './notification_error.png';
import successImage from './notification_success.png';

export const InfoMessageWrapper = styled.div`
  border-radius: 5px;
  border: 1px solid grey;
  background-color: ${props => {switch(props.type) {
    case 'error':
      return 'indianred';
    case 'success':
      return 'lightgreen';
    default:
      return 'cornflowerblue';
  }}};
  border-color: ${props => {switch(props.type) {
    case 'error':
      return 'darkred';
    case 'success':
      return 'green';
    default:
      return 'grey';
  }}};
  color: ${props => {switch(props.type) {
    case 'error':
      return 'darkred';
    case 'success':
      return 'green';
    default:
      return 'grey';
  }}};
  padding: 13px 13px 16px 40px;
  margin-top: 10px;
  margin-bottom: 10px;
  position: relative;
  
  &:before {
    content: " ";
    background-repeat: no-repeat;
    background-size: 25px 25px;
    position: absolute;
    width: 25px;
    height: 25px;
    left: 10px;
    
    background-image: ${props => {switch(props.type) {
      case 'error':
        return `url(${errorImage})`;
      case 'success':
        return `url(${successImage})`;
      default:
        return '';
    }}};
  }
`;