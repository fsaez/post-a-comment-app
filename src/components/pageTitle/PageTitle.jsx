import React from "react";
import { Wrapper, Title } from './styles';

class PageTitle extends React.Component {
  render() {
    return <Wrapper><Title>{ this.props.children }</Title></Wrapper>;
  }
}

export default PageTitle;
