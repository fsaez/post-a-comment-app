/**
 * Created by felipesaez on 23/03/18.
 */
import styled from 'styled-components';

export const Title = styled.h1`
  border-bottom: 1px solid lightgrey;
`;

export const Wrapper = styled.div`
  margin-left: auto;
  margin-right: auto;
`;