import React from "react";
import { StyledButton } from './styles';

const Button = (props) => (
  <StyledButton fullwidth={ props.fullwidth } primary={ props.primary } onClick={ props.onClick } >{ props.children }</StyledButton>
);

export default Button;
