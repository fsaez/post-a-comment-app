/**
 * Created by felipesaez on 22/03/18.
 */
import styled from 'styled-components';

export const StyledButton = styled.button`
  background-color: ${props => props.primary ? '#5875c9' : '#ededed'};
  color: ${props => props.primary ? 'white' : '#5875c9'};
  border: none;
  border-radius: 5px;
  padding: ${props => props.primary ? '10px' : '5px'};
  cursor: pointer;
  width: ${props => props.fullwidth ? '100%' : 'auto'};
  
  &:hover {
    background-color: ${props => props.primary ? 'darkblue' : '#dddddd'};
  }
`;