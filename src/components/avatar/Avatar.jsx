import React from "react";
import avatarIcon from './images/avatarIcon.png';
import { Icon, Username } from './styles';

const Avatar = (props) => (
  <div>
    <Icon
      alt={ props.username }
      src={ props.image || avatarIcon }
      width={ props.width || "60"}
      height={ props.height || "60" } />
    <Username>{ props.username }</Username>
  </div>
);

export default Avatar;
