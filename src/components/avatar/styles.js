/**
 * Created by felipesaez on 22/03/18.
 */
import styled from 'styled-components';

export const Icon = styled.img`
  display: block;
  margin-left: auto;
  margin-right: auto;
`;

export const Username = styled.div`
  overflow-wrap: normal;
  text-align: center;
  font-size: 10px;
`;