/**
 * Created by felipesaez on 23/03/18.
 */
import React from 'react';
import ReactSafeHtml from "react-safe-html";
import LoadingIcon from "components/loadingIcon";
import Avatar from 'components/avatar';
import { DetailsWrapper, ContentWrapper, Title, Description, Date, Content } from './styles';

const PostDetails = (props) => (
  <DetailsWrapper>
    <Avatar username={ props.isFetching ? <LoadingIcon /> : props.author } width="40" height="40"/>
    <ContentWrapper>
      <Title>{ props.isFetching ? <LoadingIcon /> : props.title }</Title>
      <Description>{ props.isFetching ? <LoadingIcon /> : props.description }</Description>
      <Date>{ props.isFetching ? <LoadingIcon /> : props.publishDate }</Date>
      <Content>{ props.isFetching ? <LoadingIcon /> : <ReactSafeHtml html={ props.content } /> }</Content>
    </ContentWrapper>
  </DetailsWrapper>
);

export default PostDetails;