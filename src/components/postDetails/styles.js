/**
 * Created by felipesaez on 23/03/18.
 */
import styled from 'styled-components';

export const DetailsWrapper = styled.div`
  display: flex;
  box-sizing: border-box;
  border: 1px solid #dddddd;
  border-radius: 5px;
  padding: 10px;
`;

export const ContentWrapper = styled.div`
  margin-left: 10px;
  width: 100%;
`;

export const Title = styled.div`
  font-family: Roboto,Arial,Helvetica Neue,Helvetica,sans-serif;
  font-style: normal;
  font-variant: normal;
  font-weight: 500;
  border-bottom: 1px solid #dddddd;
  color: #1f5077;
`;

export const Description = styled.div`
  color: lightgrey;
`;

export const Date = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  font-size: 10px;
`;

export const Content = styled.div`
  margin-top: 10px;
`;