import React from "react";
import AddCommentField from "./AddCommentField";
import { updateCommentUser, updateCommentContent, postAComment } from '../../actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class AddCommentFieldContainer extends React.Component {

  constructor(props) {
    super(props);

    this.onUserChange = this.onUserChange.bind(this);
    this.onContentChange = this.onContentChange.bind(this);
    this.onClickSubmit = this.onClickSubmit.bind(this);
  }

  onUserChange(e) {
    if (e) {
      this.props.updateCommentUser(e.target.value);
    }
  }

  onContentChange(e) {
    if (e) {
      this.props.updateCommentContent(e.target.value);
    }
  }

  onClickSubmit() {
    this.props.postAComment({
      commentUser: this.props.commentUser,
      commentContent: this.props.commentContent,
      selectedCommentId: this.props.selectedCommentId
    });
  }

  render() {
    return <AddCommentField
      postId={ this.props.postId }
      commentUser={ this.props.commentUser }
      commentContent={ this.props.commentContent }
      selectedCommentId={ this.props.selectedCommentId }
      inline={ this.props.inline }
      message={ this.props.message }
      onUserChange={ this.onUserChange }
      onContentChange={ this.onContentChange }
      onClickSubmit={ this.onClickSubmit }
    />;
  }
}

const mapStateToProps = (state) => ({
  commentUser: state.comments.commentUser,
  commentContent: state.comments.commentContent,
  message: state.comments.message,
  selectedCommentId: state.comments.selectedCommentId
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ updateCommentUser, updateCommentContent, postAComment }, dispatch);
};

AddCommentFieldContainer.propTypes = {
  commentUser: PropTypes.string,
  commentContent: PropTypes.string,
  message: PropTypes.object,
  updateCommentUser: PropTypes.func,
  updateCommentContent: PropTypes.func,
  postAComment: PropTypes.func,
  postId: PropTypes.string
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCommentFieldContainer);
