import React from "react";
import Button from 'components/button';
import InfoMessage from 'components/infoMessage';
import InputField from './components/inputField';
import { Wrapper } from './styles';

const AddCommentField = (props) => (
  <Wrapper inline={ props.inline }>
    <InputField
      inline={ props.inline }
      name="username"
      label="User"
      onChange={ props.onUserChange }
      value={ props.commentUser }
      smallText="Enter the username that will be used to post the comment."/>

    <InputField
      inline={ props.inline }
      name="content"
      multiline
      label="Content"
      onChange={ props.onContentChange }
      value={ props.commentContent }
      smallText="Enter the content of the comment."/>

    <Button fullwidth={ true } primary onClick={ props.onClickSubmit }>Post comment</Button>
    <InfoMessage message={ props.message } />
  </Wrapper>
);

export default AddCommentField;
