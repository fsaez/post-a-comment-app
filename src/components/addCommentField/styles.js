/**
 * Created by felipesaez on 21/03/18.
 */
import styled from 'styled-components';

export const Wrapper = styled.div`
  margin-bottom: 5px;
  margin-right: ${ props => props.inline ? '0' : 'auto'};
  margin-left: auto;
  display: block;
  padding: ${ props => props.inline ? '0' : '20px'};
  width: ${ props => props.inline ? '200px' : '300px' };
`;