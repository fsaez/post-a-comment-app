/**
 * Created by felipesaez on 24/03/18.
 */
import React from 'react';
import InputField from './InputField';

class InputFieldContainer extends React.Component {

  render() {
    const elementId = "comment-" + this.props.name;
    const placeholderText = 'Enter ' + this.props.label.toLowerCase();

    return (
      <InputField
        label={ this.props.label }
        smallText={ this.props.smallText }
        inline={ this.props.inline }
        placeholder={ placeholderText }
        id={ elementId }
        onChange={ this.props.onChange }
        type="text"
        value={ this.props.value }
        multiline={ this.props.multiline }/>
    );
  }
}

export default InputFieldContainer;