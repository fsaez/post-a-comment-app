/**
 * Created by felipesaez on 21/03/18.
 */
import styled, { css } from 'styled-components';

export const Label = styled.label`
  width: 70px;
  display: block;
  margin-bottom: 5px;
`;

const inputStyle = css`
  border: 1px solid #c8ccd0;
  font-size: 11px;
  height: 25px;
  width: 100%;
  max-width: 100%;
  min-width: 100%;
  box-sizing: border-box;
  padding: 3px 6px;
  margin-bottom: 5px;
  border-radius: 2px;
  border-width: 1px;
  border-color: #c8ccd0;
  box-shadow: none;
  color: #3b4045;
  appearance: none;
`;

export const Wrapper = styled.div`
`;

export const Input = styled.input`
  ${inputStyle}
`;

export const Textarea = styled.textarea`
  ${inputStyle}
`;

export const Small = styled.small`
  margin-bottom: 5px;
  padding: 10px;
  width: 400px;
  margin-right: 0;
  margin-bottom: 10px;
  display: block;
  font-size: 11px;
`;