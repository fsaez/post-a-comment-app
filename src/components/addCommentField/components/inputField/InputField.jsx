/**
 * Created by felipesaez on 21/03/18.
 */
import React from "react";
import { Wrapper } from './styles';
import { Textarea, Input, Label, Small } from './styles';

class InputField extends React.Component {

  render() {
    const inputProps = {
      placeholder: this.props.placeholder,
      id: this.props.elementId,
      onChange: this.props.onChange,
      type: "text",
      value: this.props.value
    };

    return (
    <Wrapper inline={ this.props.inline }>
      { !this.props.inline && <Label htmlFor={ this.props.elementId }>{ this.props.label }</Label> }
      { this.props.multiline ? <Textarea { ...inputProps }/> : <Input { ...inputProps } /> }
      { !this.props.inline && <Small>{ this.props.smallText }</Small>}
    </Wrapper>);
  }
}

export default InputField;