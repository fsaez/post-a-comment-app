/**
 * Created by felipesaez on 22/03/18.
 */
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

export const Title = styled.h3`
  margin-top: auto;
  margin-bottom: auto;
  margin-right: 10px;
  color: #eeeeee;
`;

export const Navbar = styled.div`
  list-style-type: none;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: auto;
  margin-bottom: auto;
`;

export const NavbarWrapper = styled.div`
  margin-top: 10px;
  display: flex;
  background-color: #5875c9;
  border-radius: 3px;
  padding: 5px 10px;
`;

const activeClassName = 'nav-item-active'

export const NavItem = styled(NavLink).attrs({
    activeClassName
  })`
  padding: 10px 20px;
  text-decoration: none;
  color: white;
  &:hover {
    background-color: #6E94EE;
  }
  &.${activeClassName} {
    background-color: #6885d9;
  }
`;