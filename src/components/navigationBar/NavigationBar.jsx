import React from 'react';
import { NavbarWrapper, Navbar, NavItem, Title }  from './styles';

const Navigation = () => (
  <NavbarWrapper>
    <Title>My app</Title>
      <Navbar>
        <NavItem to='/home'>Home</NavItem>
        <NavItem to='/about'>About</NavItem>
        <NavItem to='/blog'>Blog</NavItem>
      </Navbar>
  </NavbarWrapper>
);

export default Navigation;
