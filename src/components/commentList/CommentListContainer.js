import React from "react";
import CommentList from "./CommentList";
import { fetchComments, selectCommentToReply } from '../../actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AddCommentField from "components/addCommentField";

class CommentsContainer extends React.Component {

  constructor (props) {
    super(props);

    this.loadData = this.loadData.bind(this);
  }

  loadData() {
    this.props.fetchComments(this.props.postId);
  }

  getCommentElement (comment, level) {
    const isCurrentSelected = this.props.comments.selectedCommentId === comment.id;
    const addCommentField = isCurrentSelected ? <AddCommentField inline={ true }/> : null;
    const childCommentElements = !!comment.children && comment.children.map(comment => this.getCommentElement(comment, level));

    return (
      <CommentList key={ comment.id }
               level={ level }
               comment={ comment }
               onClick={ this.props.selectCommentToReply }
               addCommentField={ addCommentField }
               childCommentElements={ childCommentElements }/>
    );
  }

  render() {
    let commentElements;
    const itHasComments = this.props.comments.items && this.props.comments.items.length > 0;

    if (itHasComments) {
      commentElements = this.props.comments.items.map((comment) => {
        return this.getCommentElement(comment, 1);
      });
    }
    return (
      <div>
        { commentElements }
        <AddCommentField/>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  comments: state.comments
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ fetchComments, selectCommentToReply }, dispatch);
};

CommentsContainer.propTypes = {
  fetchComments: PropTypes.func,
  selectCommentToReply: PropTypes.func,
  comments: PropTypes.object,
  postId: PropTypes.string
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentsContainer);