import React from "react";
import Button from 'components/button';
import Avatar from 'components/avatar';
import { CommentWrapper, BubbleWrapper, AvatarWrapper, ContentWrapper , Date, Text } from './styles';

const CommentList = (props) => (
  <CommentWrapper key={ props.comment.id } level={ props.level }>
    <BubbleWrapper>
      <AvatarWrapper>
        <Avatar username={ props.comment.user } width="40" height="40"/>
      </AvatarWrapper>
      <ContentWrapper>
        <Date>{ props.comment.date }</Date>
        <Text>{ props.comment.content }</Text>
        <Button onClick={ () => { props.onClick(props.comment.id) }}>Reply</Button>
      </ContentWrapper>
    </BubbleWrapper>
    { props.addCommentField }
    { props.childCommentElements }
  </CommentWrapper>
);

export default CommentList;
