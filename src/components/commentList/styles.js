/**
 * Created by felipesaez on 22/03/18.
 */
import styled from 'styled-components';

export const CommentWrapper = styled.div`
  margin-left: ${props => (props.level) * 50 + 'px'};
`;

export const BubbleWrapper = styled.div`
  margin-top: 10px;
  margin-bottom: 10px;
  display: flex;
  
  & button {
    position: absolute;
    right: 10px;
    bottom: 10px;
  }
`;

export const AvatarWrapper = styled.div`
  width: 100px;
  max-width: 100px;
`;

export const ContentWrapper = styled.div`
  position: relative;
  background-color: #E9E9E9;
  padding: 10px;
  width: 100%;
  border-radius: 10px;
  
  &:before {
    content: "";
    position: absolute;
    top: 30%;
    margin-top: -10px;
    left: -19px;
    border: solid 10px transparent;
    border-right-color: #E9E9E9;
    z-index: 2;
  }
`;

export const Date = styled.div`
  font-size: 12px;
`;

export const Text = styled.div`
  padding: 10px;
`;