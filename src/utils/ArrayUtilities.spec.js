import ArrayUtilities from './ArrayUtilities';

describe('ArrayUtilities', () => {

  it('sorts array correctly highest first. Example b goes before a', () => {
    const inputArray = [{
      attribute: 'a'
    }, {
      attribute: 'c'
    }, {
      attribute: 'b'
    }];

    const expectedSortedArray = [{
      attribute: 'c'
    }, {
      attribute: 'b'
    }, {
      attribute: 'a'
    }];

    const actualSortedArray = ArrayUtilities.sortByAttributeHighestFirst(inputArray, 'attribute');

    expect(actualSortedArray).toEqual(expectedSortedArray);
  });
});