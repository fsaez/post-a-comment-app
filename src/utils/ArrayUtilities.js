/**
 * Method used to sort an array based on an attribute
 * @param array {Array} array to sort
 * @param attributeName {String} attribute name
 * @return {Array} sorted array
 */
const sortByAttributeHighestFirst = (array, attributeName) => (
  array.sort((a, b) => {
    const publishDateA = a[attributeName] ? a[attributeName].toString().toUpperCase() : '';
    const publishDateB = b[attributeName] ? b[attributeName].toString().toUpperCase() : '';
    if (publishDateA > publishDateB) {
      return -1;
    } else if (publishDateA < publishDateB) {
      return 1;
    } else {
      return 0;
    }
  })
);

/**
 * Method used to sort an array based on an attribute
 * @param array {Array} array to sort
 * @param attributeName {String} attribute name
 * @return {Array} sorted array
 */
const sortByAttributeLowestFirst = (array, attributeName) => (
  array.sort((a, b) => {
    const publishDateA = a[attributeName] ? a[attributeName].toString().toUpperCase() : '';
    const publishDateB = b[attributeName] ? b[attributeName].toString().toUpperCase() : '';
    if (publishDateA < publishDateB) {
      return -1;
    } else if (publishDateA > publishDateB) {
      return 1;
    } else {
      return 0;
    }
  })
);

export default {
  sortByAttributeHighestFirst: sortByAttributeHighestFirst,
  sortByAttributeLowestFirst: sortByAttributeLowestFirst
};