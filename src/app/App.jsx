/**
 * @overview Our main app layout.
 */
import React from 'react';
import PropTypes from 'prop-types';
import reducer from 'reducers/reducer';
import { createStore, applyMiddleware } from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import BlogPage from 'scenes/blogPage';
import BlogPageDetails from 'scenes/blogPage/blogPageDetails';
import Home from 'scenes/home';
import About from 'scenes/about';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import NavigationBar from 'components/navigationBar';
import { Wrapper } from './styles';

const store = createStore(
  reducer,
  applyMiddleware(thunk)
);

const App = (props) => (
  <Provider store={ store }>
      <BrowserRouter>
        <Wrapper>
          <Route path='/' component={NavigationBar}/>
          <Switch>
            <Redirect exact={true} from="/" to="/blog"/>
            <Route exact path='/about' component={About}/>
            <Route exact path='/home' component={Home}/>
            <Route exact path='/blog' component={BlogPage}/>
            <Route exact path='/blog/:postId' component={BlogPageDetails}/>
          </Switch>
        </Wrapper>
      </BrowserRouter>
  </Provider>
);

App.propTypes = {
  children: PropTypes.node,
};

export default App;
